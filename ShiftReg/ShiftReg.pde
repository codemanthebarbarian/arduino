
int data = 2;
int latch = 4;
int clock = 3;


void setup(){
  pinMode(data, OUTPUT);
  pinMode(clock, OUTPUT);
  pinMode(clock, OUTPUT);
}

void loop(){
  updateLEDs(255);
  delay(500);
  updateLEDs(0);
  delay(500);  
}

void updateLEDs(int value){
  digitalWrite(latch, LOW);
  shiftOut(data, clock, MSBFIRST, value);
  digitalWrite(latch, HIGH);
  digitalWrite(latch, LOW);
}
