/*
	Simple motor contol implementation
	
	Since: December 1, 2010
	Original Author: Darin Padlewski
*/

#include "WProgram.h"
#include "Motor.h"

Motor::Motor(int rightSpeed, int leftSpeed, int rightSpeedPin, int rightDirectionPin,
				int leftSpeedPin, int leftDirectionPin){
	_rightSpeed = rightSpeed;
	_leftSpeed = leftSpeed;
	E1 = rightSpeedPin;
	M1 = rightDirectionPin;
	E2 = leftSpeedPin;
	M2 = leftDirectionPin;
	pinMode(E1, OUTPUT);
	pinMode(E2, OUTPUT);
	pinMode(M1, OUTPUT);
	pinMode(M2, OUTPUT);
}

void Motor::stop(void){
	digitalWrite(E1,LOW);
	digitalWrite(E2,LOW);
}

void Motor::forward(void){
	analogWrite (E1,_rightSpeed);
	digitalWrite(M1,LOW);
	analogWrite (E2,_leftSpeed);
	digitalWrite(M2,LOW);
}

void Motor::forward(int rightSpeed,int leftSpeed){
	analogWrite (E1,rightSpeed);
	digitalWrite(M1,LOW);
	analogWrite (E2,leftSpeed);
	digitalWrite(M2,LOW);
}

void Motor::reverse (void){
	analogWrite (E1,_rightSpeed);
	digitalWrite(M1,HIGH);
	analogWrite (E2,_leftSpeed);
	digitalWrite(M2,HIGH);
}

void Motor::reverse (int rightSpeed,int leftSpeed){
	analogWrite (E1,rightSpeed);
	digitalWrite(M1,HIGH);
	analogWrite (E2,leftSpeed);
	digitalWrite(M2,HIGH);
}

void Motor::left (void){
	analogWrite (E1,_rightSpeed);
	digitalWrite(M1,HIGH);
	analogWrite (E2,_leftSpeed);
	digitalWrite(M2,LOW);
}

void Motor::left (int rightSpeed,int leftSpeed){
	analogWrite (E1,rightSpeed);
	digitalWrite(M1,HIGH);
	analogWrite (E2,leftSpeed);
	digitalWrite(M2,LOW);
}

void Motor::right (void){
	analogWrite (E1,_rightSpeed);
	digitalWrite(M1,LOW);
	analogWrite (E2,_leftSpeed);
	digitalWrite(M2,HIGH);
}

void Motor::right (int rightSpeed,int leftSpeed){
	analogWrite (E1,rightSpeed);
	digitalWrite(M1,LOW);
	analogWrite (E2,leftSpeed);
	digitalWrite(M2,HIGH);
}