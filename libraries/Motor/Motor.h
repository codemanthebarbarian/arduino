/*
	Simple motor contol
	
	Since: December 1, 2010
	Original Author: Darin Padlewski
*/

#ifndef Motor_h
#define Motor_h

#include "WProgram.h"

class Motor{

	private:
		int _leftSpeed; //255 is maximum speed 100 is the minimum
		int _rightSpeed;
		int E1; //M1 Speed Control
		int E2; //M2 Speed Control
		int M1; //M1 Direction Control
		int M2; //M2 Direction Control
		
	public:
		Motor(int rightSpeed, int leftSpeed, int rightSpeedPin,
				int rightDirectionPin, int leftSpeedPin, int leftDirectionPin);
		void stop(void);
		void forward(void);
		void forward(int rightSpeed,int leftSpeed);
		void reverse(void);
		void reverse (int rightSpeed,int leftSpeed);
		void left(void);
		void left (int rightSpeed,int leftSpeed);
		void right(void);
		void right(int rightSpeed,int leftSpeed);
}
	
#endif
