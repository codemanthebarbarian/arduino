#include <Wire.h>

float heading = 0;
int WriteAddress = 0x42;
int ReadAddress = 0x43;

int WriteEEPROM = 0x77; //Augment 1st Byte (binary) EEPROM Address, 2nd Byte Data
int ReadEEPROM = 0x72; //Augment 1st Byte (binary) EEPROM Address, Response 1 Byte Data
int WriteRAM = 0x47; //Augment 1st Byte (binary) address, 2nd Byte Data
int ReadRAM = 0x67; //Augment 1st Byte (binary) address, Response 1 Byte data
int Sleep = 0x53;
int Wakeup = 0x57;
int UpdateBridgeOffsets = 0x4F;
//int EnterCalibration = Ox43;
int ExitCalibration = 0x45;
int SaveOpMode = 0x4C;
int GetData = 0x41; //Response 1st byte MSB Data, 2nd byte LSB Data

void setup(){
  Wire.begin();
  Serial.begin(9600);
}

void loop(){
  heading = GetHeading();
  Serial.print("Heading: ");
  Serial.println(heading);
  delay(1000);
}

float GetHeading()
{
  byte val = 0;
  byte data[2];
  int  j, frac;

  Wire.beginTransmission(0x21);
  Wire.send(GetData); //A
  Wire.endTransmission();
  delay(8); //6000 microseconds minimum 6 ms 

  Wire.requestFrom(0x21, 2);
  j = 0;
  while(Wire.available())
  {
    char c = Wire.receive();
    data[j] = c;
    j++;
  }
  frac = data[0]*256 + data[1];
  
  return (frac/10.0);
}
