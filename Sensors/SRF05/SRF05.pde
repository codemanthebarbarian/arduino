
#define TO_CM 58
#define ECHO_PIN  5
#define TRIGGER_PIN  7
#define DELAY_US 10

unsigned long pulseLength = 0;
unsigned long distance = 0;

void setup(){
  pinMode(ECHO_PIN, INPUT);
  pinMode(TRIGGER_PIN, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  pulseLength = ReadPulse();
  Serial.print("Pulse Length: ");
  Serial.println(pulseLength);
  distance = CalcDistance(pulseLength);
  Serial.print("Distance: ");
  Serial.println(distance);
  delay(1000);
}

unsigned long ReadPulse(){
  digitalWrite(TRIGGER_PIN, HIGH);
  delayMicroseconds(DELAY_US);
  digitalWrite(TRIGGER_PIN, LOW);
  return pulseIn(ECHO_PIN, HIGH);
}

unsigned long CalcDistance(unsigned long pulseLengh){
  return pulseLength / TO_CM;
}
