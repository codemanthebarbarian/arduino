
int sensorPin = A0; // select the input pin for the potentiometer
int ledPin1 = 2; 
int ledPin2 = 3; 
int ledPin3 = 4; 
int ledPin4 = 9; 
int sensorValue = 0; // variable to store the value coming from the sensor
float voltage = 0;
float distanceCm = 0;

void setup() {
 // declare the ledPin as an OUTPUT:
 pinMode(ledPin1, OUTPUT); 
 pinMode(ledPin2, OUTPUT); 
 pinMode(ledPin3, OUTPUT); 
 pinMode(ledPin4, OUTPUT); 
 Serial.begin(9600);
}
 
void loop() {
 // read the value from the sensor:
 sensorValue = analogRead(sensorPin); 
 Serial.print("IR Reading: ");
 Serial.println(sensorValue);
 voltage = CalcVolt(sensorValue);
 Serial.print("IR Voltage: ");
 Serial.println(voltage);
 distanceCm = CalcDistance(voltage);
 Serial.print("Distance (cm): ");
 Serial.println(distanceCm);
 delay(1000); 
}

float CalcVolt(int vdc){
  return vdc * 0.00488;
}

void SetLights(){
  
}

float CalcDistance(float volt){
  if(volt < 0.958)
    return (63.0 * pow(0.34,volt))-9.0;
  else if(volt > 0.958)
    return (31.0 * pow(0.32,volt))+3.0;
  else
    return 13.5;
}
