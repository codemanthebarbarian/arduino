/* Sensor
    
   The circuit:
	* + connection of the piezo attached to analog in 0
	* - connection of the piezo attached to ground
	* 1-megohm resistor attached from analog in 0 to ground

   http://www.arduino.cc/en/Tutorial/Knock
   
   Since: October 16, 2010
   Original Author: Darin Padlewski

 */
 

// these constants won't change:
const int ledPin = 13;      // led connected to digital pin 13
const int lightSensor = A0; // The light sensor
const int tempSensor = A1;  // The temp sensor
const int threshold = 100;  // threshold value


// these variables will change:
int lightReading = 0;      // variable for light sensor
int tempReading = 0;        // variable for temperature reading
int ledState = LOW;        // variable used to store the last LED status, to toggle the light

void setup() {
 pinMode(ledPin, OUTPUT); // declare the ledPin as as OUTPUT
 Serial.begin(9600);       // use the serial port
}

void loop() {
  // read the sensor and store it in the variable sensorReading:
  lightReading = analogRead(lightSensor);
  tempReading = analogRead(tempSensor);
  
  
  // if the sensor reading is greater than the threshold:
  if (lightReading >= threshold) {
    // toggle the status of the ledPin:
    ledState = !ledState;   
    // update the LED pin itself:        
    digitalWrite(ledPin, ledState);
    // send the string "Knock!" back to the computer, followed by newline
    //Serial.println("Reading!");         
  }
  Serial.println("Reading!");  
  Serial.print("Light Sensor: ");
  Serial.println(lightReading);
  Serial.print("Temp Sensor: ");
  Serial.println(tempReading);
  delay(2000);  // delay to avoid overloading the serial port buffer
}

