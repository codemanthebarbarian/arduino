
const int buttonIn = 11;
const int gLedOut = 12;
const int rLed0 = 2;
const int rLed1 = 3;
const int rLed2 = 4;
const int rLed3 = 9;
const int MAX_STATE = 4;//the highest state number
const int CYCLE = 5000;

const boolean MOTORS = true;
const int E1 = 6; //M1 Speed Control
const int E2 = 5; //M2 Speed Control
const int M1 = 8; //M1 Direction Control
const int M2 = 7; //M2 Direction Control
const int LEFT_SPEED = 200; //255 max
const int RIGHT_SPEED = 200;
const int MAX_MOTOR_TIME = 15000;
const int MOTOR_WAIT = 5000;
boolean motorsActive = false;

int state = 0;
int buttonState = LOW;

void setup(){
  pinMode(buttonIn, INPUT);
  pinMode(gLedOut, OUTPUT);
  pinMode(rLed0, OUTPUT);
  pinMode(rLed1, OUTPUT);
  pinMode(rLed2, OUTPUT);
  pinMode(rLed3, OUTPUT);
  if (MOTORS){
    pinMode(E1, OUTPUT);
    pinMode(E2, OUTPUT);
    pinMode(M1, OUTPUT);
    pinMode(M2, OUTPUT);
    stop();
  }
  Serial.begin(9600);
  //Set the intial state of green led to off
  digitalWrite(gLedOut, LOW);
}

void loop(){
  SetState();
  //delay(5000);
}

void SetState(){
  switch (state){
  case 1:
    SetState1();
    break;
  case 2:
    SetState2();
    break;
  case 3:
    SetState3();
    break;
  case 4:
    SetState4();
    break;
  default:
    SetState0();
  }
}

void SetState0(){
  if(MOTORS) stop;
  digitalWrite(rLed0, HIGH);
  digitalWrite(rLed1, HIGH);
  digitalWrite(rLed2, HIGH);
  digitalWrite(rLed3, HIGH);
  while(true){
    CheckButton();
  }
}

void SetState1(){
  int pause = 100;
  int mTimer = 0;
  digitalWrite(rLed0, LOW);
  digitalWrite(rLed1, LOW);
  digitalWrite(rLed2, LOW);
  digitalWrite(rLed3, LOW);
  if(MOTORS) right(LEFT_SPEED, RIGHT_SPEED);
  while(true){
    digitalWrite(rLed0, HIGH);
    digitalWrite(rLed3, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed1, HIGH);
    digitalWrite(rLed0, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed2, HIGH);
    digitalWrite(rLed1, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed3, HIGH);
    digitalWrite(rLed2, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    if (MOTORS){
      if(motorsActive && mTimer > MAX_MOTOR_TIME){
        stop();
        mTimer = 0;
      }
      if(!motorsActive && mTimer > MOTOR_WAIT){ 
        right(LEFT_SPEED, RIGHT_SPEED);
        mTimer = 0;
      }
    }
  }
}

void SetState2(){
  int pause = 100;
  int mTimer = 0;
  digitalWrite(rLed0, LOW);
  digitalWrite(rLed1, LOW);
  digitalWrite(rLed2, LOW);
  digitalWrite(rLed3, LOW);
  if(MOTORS) left(LEFT_SPEED, RIGHT_SPEED);
  while(true){
    digitalWrite(rLed0, HIGH);
    digitalWrite(rLed1, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed1, HIGH);
    digitalWrite(rLed0, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed2, HIGH);
    digitalWrite(rLed1, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed3, HIGH);
    digitalWrite(rLed2, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed2, HIGH);
    digitalWrite(rLed3, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    digitalWrite(rLed1, HIGH);
    digitalWrite(rLed2, LOW);
    delay(pause);
    mTimer = mTimer + pause;
    CheckButton();
    if (MOTORS){
      if(motorsActive && mTimer > MAX_MOTOR_TIME){
        stop();
        mTimer = 0;
      }
      if(!motorsActive && mTimer > MOTOR_WAIT){ 
        left(LEFT_SPEED, RIGHT_SPEED);
        mTimer = 0;
      }
    }
  }
}

void SetState3(){
  int pause = 250;
  digitalWrite(rLed0, LOW);
  digitalWrite(rLed1, LOW);
  digitalWrite(rLed2, LOW);
  digitalWrite(rLed3, LOW);
  while(true){
    digitalWrite(rLed0, HIGH);
    digitalWrite(rLed1, HIGH);
    digitalWrite(rLed2, LOW);
    digitalWrite(rLed3, LOW);
    delay(pause);
    CheckButton();
    digitalWrite(rLed2, HIGH);
    digitalWrite(rLed3, HIGH);
    digitalWrite(rLed0, LOW);
    digitalWrite(rLed1, LOW);
    delay(pause);
    CheckButton();
  }
}

void SetState4(){
  int pause = 250;
  digitalWrite(rLed0, LOW);
  digitalWrite(rLed1, LOW);
  digitalWrite(rLed2, LOW);
  digitalWrite(rLed3, LOW);
  while(true){
    digitalWrite(rLed0, HIGH);
    digitalWrite(rLed2, HIGH);
    digitalWrite(rLed1, LOW);
    digitalWrite(rLed3, LOW);
    delay(pause);
    CheckButton();
    digitalWrite(rLed1, HIGH);
    digitalWrite(rLed3, HIGH);
    digitalWrite(rLed0, LOW);
    digitalWrite(rLed2, LOW);
    delay(pause);
    CheckButton();
  }
}

void CheckButton(){
  //Read the button
  int currentState = digitalRead(buttonIn);
  if (currentState != HIGH && buttonState == LOW){
    //Button pressed state has changed
    buttonState = HIGH;
    if(MOTORS) stop();
    digitalWrite(gLedOut, HIGH);
    //loop till button released
    while (buttonState == HIGH){
      currentState = digitalRead(buttonIn);
      if(currentState == HIGH){ 
        buttonState = LOW; //button Released
        digitalWrite(gLedOut, LOW);
      }
    }
    state++;
    if(state > MAX_STATE) state = 0;
    Serial.print("State: ");
    Serial.println(state);
    SetState();
  }
}

void stop(void){
  digitalWrite(E1,LOW);
  digitalWrite(E2,LOW);
  motorsActive = false;
}

void forward(int a,int b){
  analogWrite (E1,a);
  digitalWrite(M1,LOW);
  analogWrite (E2,b);
  digitalWrite(M2,LOW);
  motorsActive = true;
}

void reverse (int a,int b){
  analogWrite (E1,a);
  digitalWrite(M1,HIGH);
  analogWrite (E2,b);
  digitalWrite(M2,HIGH);
  motorsActive = true;
}

void left (int a,int b){
  analogWrite (E1,a);
  digitalWrite(M1,HIGH);
  analogWrite (E2,b);
  digitalWrite(M2,LOW);
  motorsActive = true;
}

void right (int a,int b){
  analogWrite (E1,a);
  digitalWrite(M1,LOW);
  analogWrite (E2,b);
  digitalWrite(M2,HIGH);
  motorsActive = true;
}


