
int ledRed = 5;
int ledGreen = 6;
int ledBlue = 7;
int state = 0;

void setup(){
  Serial.begin(9600);
  Serial.println("Entering Setup");
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(ledBlue, OUTPUT);
  attachInterrupt(0, interrupt0, LOW);//PullUp
  attachInterrupt(1, interrupt1, RISING);//PullDown
  digitalWrite(ledRed, LOW);
  digitalWrite(ledGreen, LOW);
  digitalWrite(ledBlue, LOW);
  Serial.println("Exiting Setup");
}

void loop(){
}

void interrupt0(){
  Serial.println("Entering interrupt0");
  switch(state){
    case 0:
      state = 1;
      setState();
      break;
    case 1:
      state = 2;
      setState();
      break;
    case 2:
      state = 3;
      setState();
      break;
    default:
      state = 1;
      setState();
      break;
  }
  Serial.println("Exiting interrupt0");
}

void interrupt1(){
  Serial.println("Entering interrupt1");
  state = 0;
  setState();
  Serial.println("Exiting interrupt1");
}

void setState(){
  switch(state){
    case 0:
      digitalWrite(ledRed, LOW);
      digitalWrite(ledGreen, LOW);
      digitalWrite(ledBlue, LOW);
      break;
    case 1:
      digitalWrite(ledRed, HIGH);
      digitalWrite(ledGreen, LOW);
      digitalWrite(ledBlue, LOW);
      break;
    case 2:
      digitalWrite(ledRed, LOW);
      digitalWrite(ledGreen, HIGH);
      digitalWrite(ledBlue, LOW);
      break;
    default:
      digitalWrite(ledRed, LOW);
      digitalWrite(ledGreen, LOW);
      digitalWrite(ledBlue, HIGH);
      break;
  }
}
