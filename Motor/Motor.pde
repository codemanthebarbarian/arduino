/*
Title: DFRobotShop Rover Sample Sketch #1
Authors: DFRobot, RobotShop
Date: 12/03/2010
Licence: GPL v3
Description: Sketch for the DFRobotShop Rover.
URL: www.robotshop.com
This program allows serial commands to be sent to the DFRobotShop Rover by either a wired (USB) or
wireless (Bluetooth, XBee) connection. The commands are "w", "a", "s" and "d" for driving forward,
turning left, turning right and reversing. The rover will execute the command until it is told to stop by
pressing any other character.
We suggest using the DFRobot Bluetooth module or XBee module with Windows Hyperterminal for fluid
wireless control. Ensure you select the correct COM port and 9600 baud rate. Note that it is likely the
two motors are not identical and you will need to adjust the speed so the robot goes perfectly straight.
*/

int E1 = 6; //M1 Speed Control
int E2 = 5; //M2 Speed Control
int M1 = 8; //M1 Direction Control
int M2 = 7; //M2 Direction Control
void setup(void)
{
int i;
for(i=5;i<=8;i++)
pinMode(i, OUTPUT);
Serial.begin(9600);
}
void loop(void)
{
//while (Serial.available() < 1) {} // Wait until a character is received
//char val = Serial.read();
int leftspeed = 100; //255 is maximum speed
int rightspeed = 100;
delay(5000);
Serial.println("Right Forward");
analogWrite (E2,rightspeed);
digitalWrite(M2,LOW);
delay(5000);
Serial.println("Right Stop");
analogWrite (E2,0);
digitalWrite(M2,LOW);
delay(5000);
Serial.println("Right Reverse");
analogWrite (E2,rightspeed);
digitalWrite(M2,HIGH);
delay(5000);
Serial.println("Right Stop");
analogWrite (E2,0);
digitalWrite(M2,HIGH);


//switch(val) // Perform an action depending on the command
//  {
//  case 'w'://Move Forward
//  forward (leftspeed,rightspeed);
//  break;
//  case 's'://Move Backwards
//  reverse (leftspeed,rightspeed);
//  break;
//  case 'a'://Turn Left
//  left (leftspeed,rightspeed);
//  break;
//  case 'd'://Turn Right
//  right (leftspeed,rightspeed);
//  break;
//  default:
//  stop();
//  break;
//  }
}

void stop(void) //Stop
{
digitalWrite(E1,LOW);
digitalWrite(E2,LOW);
}
void forward(char a,char b)
{
analogWrite (E1,a);
digitalWrite(M1,LOW);
analogWrite (E2,b);
digitalWrite(M2,LOW);
}
void reverse (char a,char b)
{
analogWrite (E1,a);
digitalWrite(M1,HIGH);
analogWrite (E2,b);
digitalWrite(M2,HIGH);
}
void left (char a,char b)
{
analogWrite (E1,a);
digitalWrite(M1,HIGH);
analogWrite (E2,b);
digitalWrite(M2,LOW);
}
void right (char a,char b)
{
analogWrite (E1,a);
digitalWrite(M1,LOW);
analogWrite (E2,b);
digitalWrite(M2,HIGH);
}
